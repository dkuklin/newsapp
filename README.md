**TO START APP FOLLOW THIS FEW SIMPLE STEPS**

---

## Clone a repository

1. Clone repository with NewsApp from URL https://dkuklin@bitbucket.org/dkuklin/newsapp.git

---

## Create project with pods

1. Opend command line.
2. Move to cloned folder NewsApp.
3. Build a project with command: 'pod install'

---

## Start project

1. After the project is ready open NewsApp.xcworkspace in Xcode
2. Start the App

---

** FAQ **

## How to install pods ##

Probably you should install cocoapods before creating project
All necessary information you can find here: [Install cocoapods](https://cocoapods.org/)

## How to clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).