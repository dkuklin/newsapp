//
//  Configuration.swift
//  PlayerTestApp
//
//  Created by Dmitry Kuklin on 02.11.2017.
//  Copyright © 2017 DK. All rights reserved.
//

import Foundation
import UIKit

class Configuration {
    
    // MARK: - Constants
    private(set) static var SearchMenuToMove: CGFloat = 40.0
    private(set) static var CellHeight: CGFloat = 120.0
    
    // MARK: -  STRINGS
    private(set) static var shareURL = "https://laravel-news.com/"
    private(set) static var NewsCellIdentifire = "NewsCellIdentifire"

    // MARK: - XIB identificators
    private(set) static var SearchView = "SearchView"
    private(set) static var ShortNewsCell = "ShortNewsCell"
    
    // MARK: - Segue identificators
    private(set) static var FromFeedToNew = "FullNews"

}

