//
//  WebContentCreator.swift
//  NewsApp
//
//  Created by Dmitry Kuklin on 15.12.2017.
//  Copyright © 2017 DK. All rights reserved.
//

import Foundation

class WebContentCreator {
    // MARK: Creating an HTML doc from few string parametrs
    func createWebContent(_ title: String, author: String, date: String, content: String, img: String) -> String {
        let contentForWebViewString = "<h2>\(title)</h2><p align=right>\(author)<br>\(date)</p> <br> <img src=\(img) align=middle width='100%' > <br> \(content)"
        return contentForWebViewString
    }
    
}
