//
//  ExtensionString.swift
//  AlphaBank
//
//  Created by Aliaksei Danilchanka on 12.04.16.
//  Copyright © 2016 Alfa-Bank. All rights reserved.
//

import Foundation

public extension String {

    // Convert String to Date with format
    func toDate(_ format: String? = "yyyy-MM-dd") -> Date? {
        let text = self
        let dateFmt = DateFormatter()
        dateFmt.timeZone = TimeZone.current
        if let fmt = format {
            dateFmt.dateFormat = fmt
        }
        return dateFmt.date(from: text)
    }
    
    // Convert formated HTML to string
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType:  NSAttributedString.DocumentType.html], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    
}
