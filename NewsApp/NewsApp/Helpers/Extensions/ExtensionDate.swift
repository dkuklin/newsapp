//
//  ExtensionNSDate.swift
//  AlphaBank
//
//  Created by Alex Kazinets on 12/01/2017.
//  Copyright © 2017 Alfa-Bank. All rights reserved.
//

let commonDatePattern = "yyyy-MM-dd'T'HH:mm:ssZ"
let dateWithoutTimePattern = "yyyy-MM-dd"
let dateWithoutTimePatternInversed = "dd.MM.yyyy"
let dateWithoutTimePatternSpaced = "dd MMMM yyyy"
let fullDateHourMinutePattern = "dd.MM.yyyy, HH:mm"

import Foundation

extension Date {
    
    // MARK: Convert Date to string with no time format
    func dateWithoutTime() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateWithoutTimePattern
        return dateFormatter.string(from: self)
    }
}
