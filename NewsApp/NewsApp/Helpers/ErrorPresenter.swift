//
//  ErrorPresenter.swift
//  NewsApp
//
//  Created by Dmitry Kuklin on 18.12.2017.
//  Copyright © 2017 DK. All rights reserved.
//

import Foundation
import UIKit

class ErrorPresenter {
    // Alert view with Error message 
    static func errorAlertShow(_ controller: UIViewController, errorMessage: String) {
        let alertView = UIAlertController(title: "ERROR!", message: "\(errorMessage)", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertView.addAction(action)
        controller.present(alertView, animated: true, completion: nil)
    }
}
