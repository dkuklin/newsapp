//
//  NewsArchive.swift
//  NewsApp
//
//  Created by Dmitry Kuklin on 18.12.2017.
//  Copyright © 2017 DK. All rights reserved.
//

import Foundation
import RealmSwift

class NewsArchive: Object {
    
    @objc dynamic var archive_id : String = "182"
    var archive = List<NewsModel>()
    
    override class func primaryKey() -> String? { return "archive_id" }
    
}
