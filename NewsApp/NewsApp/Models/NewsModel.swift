//
//  NewsModel.swift
//  NewsApp
//
//  Created by Dmitry Kuklin on 12.12.2017.
//  Copyright © 2017 DK. All rights reserved.
//

import Foundation
import RealmSwift

class NewsModel: Object, Codable {
    // MARK: News info
    @objc dynamic var id: Int = 0
    @objc dynamic var title: String = ""
    @objc dynamic var url: String = ""
    @objc dynamic var image: String = ""
    @objc dynamic var content_html: String = ""
    @objc dynamic var date_published: String = ""
    @objc dynamic var date_modified: String = ""
    @objc dynamic var author: Author?
    
}

// MARK: - Supporting models

class Author: Object, Codable{
    @objc dynamic var name: String = ""
}

