//
//  ShortNewsCell.swift
//  NewsApp
//
//  Created by Dmitry Kuklin on 12.12.2017.
//  Copyright © 2017 DK. All rights reserved.
//

import UIKit

class ShortNewsCell: UITableViewCell {

    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var newsContentLbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    
    func configureCell(newsModel: NewsModel){
        titleLbl.text = newsModel.title
        newsContentLbl.text = newsModel.content_html.htmlToString
        let date = newsModel.date_published.toDate(commonDatePattern)
        dateLbl.text = date?.dateWithoutTime()
    }
}
