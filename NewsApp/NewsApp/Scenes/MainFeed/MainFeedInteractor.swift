//
//  MainFeedInteractor.swift
//  NewsApp
//
//  Created by Dmitry Kuklin on 12.12.2017.
//  Copyright (c) 2017 DK. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol MainFeedBusinessLogic{
    func fetchNewsFeed(request: MainFeed.FetchNews.Request)
    func saveNewsToDB(_ news: [NewsModel])
}

protocol MainFeedDataStore{
    var news: [NewsModel]? { get }
}

class MainFeedInteractor: MainFeedBusinessLogic, MainFeedDataStore {
    var presenter: MainFeedPresentationLogic?
    var worker = MainFeedWorker()
    var news: [NewsModel]?

    // MARK: Fetch news feed

    func fetchNewsFeed(request: MainFeed.FetchNews.Request) {
        worker.fetchNewsFromInternet(completionHandler: { (newsArray) in
            self.news = newsArray
            let newsResponse = MainFeed.FetchNews.Response(news: newsArray, error: nil)
            self.presenter?.presentFetchedNews(response: newsResponse)
        }) { (error) in
            self.worker.fetchNewsFromDB(completionHandler: { (cachedNews) in
                self.news = cachedNews
                let cachedNewsResponse = MainFeed.FetchNews.Response(news: cachedNews, error: nil)
                self.presenter?.presentFetchedNews(response: cachedNewsResponse)
            }, failure: { (error) in
                let errorResponse = MainFeed.FetchNews.Response(news: nil, error: error?.localizedDescription)
                self.presenter?.presentFetchedNews(response: errorResponse)
            })
            let errorResponse = MainFeed.FetchNews.Response(news: nil, error: error?.userInfo["NSLocalizedDescription"] as? String)
            self.presenter?.presentFetchedNews(response: errorResponse)
            
        }
    }
    
    func saveNewsToDB(_ news: [NewsModel]){
        worker.saveNewsToDB(news, completionHandler: { (isSavedOk) in
            print("news has been saved")
        }) { (error) in
            let errorResponse = MainFeed.FetchNews.Response(news: nil, error: error?.localizedDescription)
            self.presenter?.presentErrorAlert(response: errorResponse)
        }
    }
}
