//
//  SearchView.swift
//  NewsApp
//
//  Created by Dmitry Kuklin on 18.12.2017.
//  Copyright © 2017 DK. All rights reserved.
//

import UIKit

class SearchView: UIView {

    @IBOutlet var containerView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed(Configuration.SearchView, owner: self, options: nil)
        addSubview(containerView)
        containerView.frame = self.bounds
        containerView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
}
