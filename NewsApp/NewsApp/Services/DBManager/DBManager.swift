//
//  DBManager.swift
//  NewsApp
//
//  Created by Dmitry Kuklin on 18.12.2017.
//  Copyright © 2017 DK. All rights reserved.
//

import Foundation
import RealmSwift

class DBManager : Object {
    
    static let userIDValue: String = "182"
    static let shared = DBManager()
    
    static var realm: Realm {
        get {
            do {
                let realm = try Realm()
                return realm
            }
            catch {
                print("Could not access database: ", error)
            }
            return self.realm
        }
    }
    
    // MARK: Get news from local DB
    func fetchDBNews(success:@escaping ([NewsModel]) -> Void, failure: @escaping (Error?) -> Void){
        var allNews = [NewsModel]()
        
        do {
            DBManager.realm.beginWrite()
        
            let news: NewsArchive? = DBManager.realm.object(ofType: NewsArchive.self, forPrimaryKey: DBManager.userIDValue)
            if let news = news {
                allNews = Array(news.archive)
                print("=== \(news.archive.count) ===")
            }
            try DBManager.realm.commitWrite()
            
            success(allNews)
        } catch {
            failure(error)
        }
    }
    
    // MARK: Save news to local DB
    func saveNews(_ news : [NewsModel], success:@escaping (Bool) -> Void, failure: @escaping (Error?) -> Void) {
        do {
            DBManager.realm.beginWrite()
            
            var archiveModel = DBManager.realm.object(ofType: NewsArchive.self, forPrimaryKey: DBManager.userIDValue)
            if archiveModel == nil {
                archiveModel = NewsArchive()
                archiveModel!.archive_id = DBManager.userIDValue
            } else {
                archiveModel?.archive.removeAll()
            }
            
            print("*** \(archiveModel!.archive.count) ***")
            for item in news{
                archiveModel!.archive.append(item)
            }
            print("!!! \(archiveModel!.archive.count) !!!")
            
            DBManager.realm.add(archiveModel!, update: true)
            try DBManager.realm.commitWrite()
            success(true)
        } catch {
            failure(error)
        }
    }
}
