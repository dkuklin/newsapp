//
//  ApiManager.swift
//  PlayerTestApp
//
//  Created by Dmitry Kuklin on 02.11.2017.
//  Copyright © 2017 DK. All rights reserved.
//

import Foundation

fileprivate class ApiCommonManadger {
    static let shared = ApiCommonManadger()
    
    // MARK: - Common methods to send send / get request / response
    
    func sendRequest(request: NSMutableURLRequest, failure: @escaping (NSError?) -> Void, resultHandler: @escaping ([Any]) -> Void) {
        print("************ REQUEST HAS BEEN SENT **************")
        URLSession.shared.dataTask(with: request as URLRequest) {(data, response, error) in
            if (data != nil) {
                if let json = try?  JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? [String : Any] {
                    if let result = json!["items"] as? [Any] {
                        resultHandler(result)
                        print("************ REQUEST HAS BEEN GOTTEN **************")
                    }
                    
                    // ELSE JSON not serialized
                } else {
                    DispatchQueue.main.async() {
                        failure(error as NSError?)
                    }
                }
                // ELSE Data == nil
            } else {
                DispatchQueue.main.async() {
                    failure(error as NSError?)
                }
            }
            }.resume()
    }
}

// MARK: - API class to get News
class ApiNewsManager {
    static let shared = ApiNewsManager()
    
    // MARK: - GET requests
    
    // MARK: - get News feed
    func getNewsFeed(success:@escaping ([NewsModel]) -> Void, failure: @escaping (NSError?) -> Void) {
        let methodName = "feed/json"
        let urlString = "\(Configuration.shareURL)\(methodName)"
        print(urlString)
        let url = NSURL(string: urlString)!
        
        let request:NSMutableURLRequest = NSMutableURLRequest(url:url as URL)
        request.httpMethod = "GET"
        request.addValue("text/plain", forHTTPHeaderField: "Content-Type")
        
        ApiCommonManadger.shared.sendRequest(request: request, failure: { (error) in
            failure(error)
        }) { (json) in
            let decoder = JSONDecoder()
            if let newsArray = json as? [[String: Any]] {
                if let jsonData : Data = try? JSONSerialization.data(withJSONObject: newsArray, options: JSONSerialization.WritingOptions.prettyPrinted) {
                    let news = try! decoder.decode([NewsModel].self, from: jsonData)
                    DispatchQueue.main.async() {
                        success(news)
                    }
                }
            }
        }
    }
}
